# jenkins-build-scripts #

This repo contains the build scripts used for the Continuous Integration pipelines configured in Jenkins CI Server

## What is this repository for? ##

This provides config control of the script call by Jenkins when a Developer pushes to the branch that Jenkins is configured to build on.
These scripts were previously uncontrolled.