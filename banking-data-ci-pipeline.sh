node {
    
    def commit_id = ""
    def code_branch_name = ''
    def devops_branch_name = ''
    def test_result = ''
    stage('Init'){
        code_branch_name = 'develop'
		code_branch_name_shortname = 'develop'
        devops_branch_name = 'master'
    }

  stage('Checkout DevOps'){
    dir('devops') { // switch to subdir
        git branch: devops_branch_name, url: 'git@bitbucket.org:barbondevops/microservices.git'
            
    }
  }

  dir('code') { // switch to subdir
    stage('Checkout Code'){
        try {
            sh '([ -d .git ] && git  reset --hard) '
        } catch (error) {
            
        }

        git branch: code_branch_name, url: 'git@bitbucket.org:barbondev/banking-data.git'
        sh 'git rev-parse HEAD > commit'
        commit_id = readFile('commit').trim()
        sh "sed -i -e \"s|{DOCKER_IMAGE_VERSION}|$code_branch_name-$commit_id|\" -e \"s|{COMMIT_ID}|$commit_id|\" -e \"s|{BRANCH}|$code_branch_name|\" public/test.php"
        sh "sed -i -e \"s|{DOCKER_IMAGE_VERSION}|$code_branch_name-$commit_id|\" -e \"s|{COMMIT_ID}|$commit_id|\" -e \"s|{BRANCH}|$code_branch_name|\" public/version.json"
        //sh 'rm -f sed*'
    }
    
    stage('Build and Deploy') {
        sh 'chmod +x ./../devops/DevelopmentECSDeploymnetScripts/ecs_deploy.sh'
        sh 'cp ~/.ssh/id_rsa conf/jenkins_bitbucket'
    
        sh "./../devops/DevelopmentECSDeploymnetScripts/ecs_deploy.sh banking-data $code_branch_name_shortname-$commit_id"
        sh "sleep 180s"
    }

    /*
    stage('Test'){
        sh 'newman run ./test/postman-utility-server.json -e ./test/Staging.postman_environment.json; echo $? > status'
        test_result = readFile('status').trim()
    }
    */
  }
  
}