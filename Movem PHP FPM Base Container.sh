node {
    
    def code_branch_name = ''
    def devops_branch_name = ''
    def container_version = ''
	def codeCredentialsId = 'bitbucket-ci'
    
    stage('Init'){
        code_branch_name = 'develop'
        devops_branch_name = 'master'
        container_version = 'latest'
    }

  stage('Checkout DevOps'){
    dir('devops') { // switch to subdir
        git branch: devops_branch_name, url: 'git@bitbucket.org:barbondevops/microservices.git'
            
    }
  }

  dir('code') { // switch to subdir
    stage('Checkout Code'){
        try {
            sh '([ -d .git ] && git  reset --hard) '
        } catch (error) {
            
        }

        git branch: code_branch_name, url: 'git@bitbucket.org:movem/php-fpm-container.git', credentialsId: codeCredentialsId
    }
    
    stage('Build and Push') {
        sh 'chmod +x ./../devops/DevelopmentECSDeploymnetScripts/movem_container_build.sh'
    
        sh "./../devops/DevelopmentECSDeploymnetScripts/movem_container_build.sh movem-php-fpm-container $container_version"
    }

  }
  
}
