node {
    
    def commit_id = ""
    def code_branch_name = ''
    def devops_branch_name = ''
    def test_result = ''
    stage('Init'){
        code_branch_name = 'develop'
		code_branch_name_shortname = 'develop'
        devops_branch_name = 'master'
    }

  stage('Checkout DevOps'){
    dir('devops') { // switch to subdir
        git branch: devops_branch_name, url: 'git@bitbucket.org:barbondevops/microservices.git'
            
    }
  }

  dir('code') { // switch to subdir
    stage('Checkout Code'){
        try {
            sh '([ -d .git ] && git  reset --hard) '
        } catch (error) {
            
        }

        git branch: code_branch_name, url: 'git@bitbucket.org:barbondev/barboncore.services.docker.git'
        sh 'git rev-parse HEAD > commit'
        commit_id = readFile('commit').trim()
        //sh "sed -i -e \"s|{DOCKER_IMAGE_VERSION}|$code_branch_name-$commit_id|\" -e \"s|{COMMIT_ID}|$commit_id|\" -e \"s|{BRANCH}|$code_branch_name|\" public/test.php"
        sh "sed -i -e \"s|{DOCKER_IMAGE_VERSION}|$code_branch_name-$commit_id|\" -e \"s|{COMMIT_ID}|$commit_id|\" -e \"s|{BRANCH}|$code_branch_name|\" conf/version.json"
        //sh 'rm -f sed*'
    }
    
    stage('Build and Deploy') {
        sh 'chmod +x ./../devops/DevelopmentECSDeploymnetScripts/ecs_deploy_dot_net.sh'
        sh 'cp ~/.ssh/id_rsa conf/jenkins_bitbucket'
    
        sh "./../devops/DevelopmentECSDeploymnetScripts/ecs_deploy_dot_net.sh barboncore.services.docker $code_branch_name_shortname-$commit_id"
        sh "sleep 180s"
    }

  }
  
}